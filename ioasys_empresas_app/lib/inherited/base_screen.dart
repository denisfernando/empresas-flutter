import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/library/util.dart';
import 'package:ioasys_empresas_app/widget/geral/app_bar_widget.dart';

class BaseScreen extends StatelessWidget {
  final Widget title;
  final Widget subtitle;
  final Widget leadingAppbarIconButton;
  final Widget suffixAppBarIconButton;
  final Widget body;
  final bool showAppBar;
  final bool automaticallyImplyLeading;
  final Widget bottomNavigationBar;
  final Widget fab;
  final Color colorAppBar;

  const BaseScreen({
    @required this.body,
    this.title,
    this.subtitle,
    this.leadingAppbarIconButton, 
    this.suffixAppBarIconButton, 
    this.showAppBar: true,
    this.automaticallyImplyLeading: true,
    this.bottomNavigationBar,
    this.fab,
    this.colorAppBar,
  });
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: showAppBar ? AppBarWidget(
        appBarColor: colorAppBar,
        title: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 36),
          child: title ?? Image(
            height: 30,
            image: AssetImage(Util.assetsLogo),
            fit: BoxFit.fitWidth,
            alignment: Alignment.center,
          ),
        ),
        subtitleWidget: subtitle,
        automaticallyImplyLeading: automaticallyImplyLeading,
        leadingAppbarIconButton: leadingAppbarIconButton,
        suffixAppBarIconButton: Row(
          children: [
            
          ],
        ),
      ) : null,
      body: body,
      bottomNavigationBar: bottomNavigationBar,
      floatingActionButton: fab,
    );
  }
}