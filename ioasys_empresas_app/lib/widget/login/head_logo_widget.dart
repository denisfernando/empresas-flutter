import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/library/theme_helper.dart';
import 'package:ioasys_empresas_app/library/util.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:flutter/widgets.dart';

class RPSCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var rect = Offset.zero & size;
    Paint paint_0 = new Paint()
      ..style = PaintingStyle.fill
      ..strokeWidth = 1
      ..shader = LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: ThemeHelper.instance.colorsGradient,
      ).createShader(rect);

    Path path_0 = Path();
    path_0.moveTo(0, size.height * 0.0057143);
    path_0.quadraticBezierTo(
        0, size.height * 0.4957143, 0, size.height * 0.5935714);
    path_0.cubicTo(
        size.width * 0.3121875,
        size.height * 0.9757143,
        size.width * 0.6881250,
        size.height * 0.9792857,
        size.width,
        size.height * 0.5935714);
    path_0.quadraticBezierTo(size.width * 1.0065625, size.height * 0.4350000,
        size.width * 0.9987500, size.height * 0.0057143);
    path_0.lineTo(0, size.height * 0.0057143);
    path_0.close();

    canvas.drawPath(path_0, paint_0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class HeadLogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        CustomPaint(
          size: Size(
              MediaQuery.of(context).size.width,
              (MediaQuery.of(context).size.width * 0.4375)
                  .toDouble()), //You can Replace [WIDTH] with your desired width for Custom Paint and height will be calculated automatically
          painter: RPSCustomPainter(),
        ),
        _buildLogoETexto(context),
      ],
    );
  }

  Widget _buildLogoETexto(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            height: 60,
            child: Image.asset(
              Util.assetsLogo,
              fit: BoxFit.fitHeight,
              alignment: Alignment.center,
              //color: ThemeHelper.instance.colorSecondary,
            ),
          ),
          Text(
            UtilText.msgWellcome,
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }
}
