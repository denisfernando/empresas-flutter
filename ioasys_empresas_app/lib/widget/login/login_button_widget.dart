import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/bloc/login_bloc.dart';
import 'package:ioasys_empresas_app/enum/enum_status_login.dart';
import 'package:ioasys_empresas_app/library/theme_helper.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:ioasys_empresas_app/widget/geral/progress_indicator_widget.dart';
import 'package:provider/provider.dart';

class LoginButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final loginBloc = Provider.of<LoginBloc>(context);
    return Container(
      height: 46,
      margin: EdgeInsets.fromLTRB(loginBloc.horizontalPadding, 12, loginBloc.horizontalPadding, 30),
      child: StreamBuilder<EnumStatusLogin>(
        stream: loginBloc.streamEnabledLogin,
        builder: (context, snapshot) {
          final caseAux = snapshot.data ?? EnumStatusLogin.none;
          final enabled = caseAux == EnumStatusLogin.habilitado;
          final isProcessando = caseAux == EnumStatusLogin.processando;
          return RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
            child: !isProcessando ? Text(
              UtilText.labelButtonAccess, 
              style: TextStyle(
                color: ThemeHelper.instance.textTheme.overline.color,
                fontSize: 16,
              ),
            ) : Center(
              child: ProgressIndicatorWidget(),
            ),
            onPressed: enabled ? () {
              loginBloc.autenticarUsuario(context);
            } : null,
          );
        }
      ),
    );
  }
}