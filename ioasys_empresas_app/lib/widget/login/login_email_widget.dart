import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ioasys_empresas_app/bloc/login_bloc.dart';
import 'package:ioasys_empresas_app/library/util_style.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:provider/provider.dart';

class LoginEmailWidget extends StatelessWidget {
  
  final _keyFormEmail = GlobalKey<FormState>();
  final TextEditingController _controllerEmail;
  final TextEditingController _controllerSenha;
  final FocusNode _nodeEmail;
  final FocusNode _nodeSenha;

  LoginEmailWidget(this._controllerEmail, this._controllerSenha, this._nodeEmail, this._nodeSenha) {
    _nodeEmail?.addListener(_handlerFocus);
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<LoginBloc>(context);
    return Form(
      key: _keyFormEmail,
      child: TextFormField(
        focusNode: _nodeEmail,
        controller: _controllerEmail,
        textInputAction: TextInputAction.done,
        textCapitalization: TextCapitalization.none,
        decoration: UtilStyle.inputDecorationLogin(UtilText.labelLogin, _buildIcon()),
        style: UtilStyle.textFormStyleLogin(),
        onChanged: (value){
          _onChanged(bloc, value);
        },
        onFieldSubmitted: (_){
          _onFieldSubmitted(context);
        },
        validator: bloc.emailValidator,
        keyboardType: TextInputType.emailAddress,
      ),
    );
  }

  Widget _buildIcon() {
    return Icon(
      FontAwesomeIcons.solidUserCircle,
      color: UtilStyle.colorPrincipal,
      size: 24,
    );
  }

  void _onFieldSubmitted(BuildContext context) {
    if (_controllerEmail.text.isEmpty){
      _validateForm();
    } else {
      if (_controllerSenha.text.isEmpty){
        FocusScope.of(context).requestFocus(_nodeSenha);
      }
    }
  }

  void _onChanged(LoginBloc bloc, String value) {
    bloc.attUsuario(email: value);
  }

  void _handlerFocus() {
    if (!_nodeEmail.hasFocus) {
      _validateForm();
    }
  }

  void _validateForm() {
    _keyFormEmail?.currentState?.validate();
  }
}