import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ioasys_empresas_app/bloc/login_bloc.dart';
import 'package:ioasys_empresas_app/library/util_style.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:provider/provider.dart';

class LoginSenhaWidget extends StatelessWidget {
  
  final _keyFormSenha = GlobalKey<FormState>();
  final TextEditingController _controllerSenha;
  final TextEditingController _controllerEmail;
  final FocusNode _nodeSenha;
  final FocusNode _nodeEmail;

  LoginSenhaWidget(this._controllerSenha, this._controllerEmail, this._nodeSenha, this._nodeEmail) {
    _nodeSenha?.addListener(_handlerFocus);
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<LoginBloc>(context);
    return Form(
      key: _keyFormSenha,
      child: StreamBuilder<bool>(
        stream: bloc.streamVisualizarSenha,
        builder: (_, snapshot) {
          final visualiza = snapshot.data ?? true;
          return TextFormField(
            focusNode: _nodeSenha,
            controller: _controllerSenha,
            textInputAction: TextInputAction.done,
            textCapitalization: TextCapitalization.none,
            decoration: UtilStyle.inputDecorationLogin(UtilText.labelPassword, _buildVisualizarSenha(visualiza, bloc)),
            style: UtilStyle.textFormStyleLogin(),
            obscureText: visualiza,
            onChanged: (value){
              _onChangedText(bloc, value);
            },
            onFieldSubmitted: (_){
              _onFieldSubmitted(context);
            },
            validator: bloc.senhaValidator,
            keyboardType: TextInputType.visiblePassword,
          );
        }
      ),
    );
  }

  Widget _buildVisualizarSenha(bool visualiza, LoginBloc bloc) {
    return InkWell(
      borderRadius: BorderRadius.circular(30),
      child: Icon(
        visualiza ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
        size: 22,
        color: UtilStyle.colorPrincipal,
      ),
      onTap: (){
        _changeObscureText(bloc, !visualiza);
      },
    );
  }

  void _onFieldSubmitted(BuildContext context) {
    if (_controllerSenha.text.isEmpty){
      _validateForm();
    } else {
      if (_controllerEmail.text.isEmpty){
        FocusScope.of(context).requestFocus(_nodeEmail);
      }
    }
  }

  void _onChangedText(LoginBloc bloc, String value) {
    bloc.attUsuario(senha: value);
  }

  void _changeObscureText(LoginBloc bloc, bool value) {
    bloc.sinkVisualizarSenha(value);
  }

  void _handlerFocus() {
    if(!_nodeSenha.hasFocus) {
      _validateForm();
    }
  }

  void _validateForm() {
    _keyFormSenha?.currentState?.validate();
  }
}