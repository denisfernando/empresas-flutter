import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/bloc/login_bloc.dart';
import 'package:provider/provider.dart';

import 'login_email_widget.dart';
import 'login_senha_widget.dart';

class LoginFormWidget extends StatefulWidget {
  @override
  _LoginFormWidgetState createState() => _LoginFormWidgetState();
}

class _LoginFormWidgetState extends State<LoginFormWidget> {
  TextEditingController _controllerSenha;
  TextEditingController _controllerEmail;
  FocusNode _nodeEmail;
  FocusNode _nodeSenha;

  @override
  void initState() {
    super.initState();
    _controllerSenha = TextEditingController();
    _controllerEmail = TextEditingController();
    _nodeEmail = FocusNode();
    _nodeSenha = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<LoginBloc>(context);
    return Padding(
      padding: EdgeInsets.fromLTRB(bloc.horizontalPadding, 30, bloc.horizontalPadding, 6),
      child: Column(
        children: <Widget>[
          LoginEmailWidget(_controllerEmail, _controllerSenha, _nodeEmail, _nodeSenha),
          Padding(
            padding: const EdgeInsets.only(top: 26, bottom: 20),
            child: LoginSenhaWidget(_controllerSenha, _controllerEmail, _nodeSenha, _nodeEmail),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _controllerSenha?.dispose();
    _controllerEmail?.dispose();
    _nodeEmail?.dispose();
    _nodeSenha?.dispose();
    super.dispose();
  }
}
