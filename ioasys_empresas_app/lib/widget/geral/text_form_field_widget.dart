import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ioasys_empresas_app/library/theme_helper.dart';
import 'package:ioasys_empresas_app/library/util_style.dart';

class TextFormFieldWidget extends StatelessWidget {
  final List<TextInputFormatter> _listTextInput = [];

  final TextEditingController controller;
  final String initialValue;
  final TextInputType textInputType;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final TextInputFormatter textInputFormatter;
  final Function(String value) funcValidator;
  final Function onEditingComplete;
  final Function(String value) onFieldSubmitted;
  final Function(String value) onChanged;
  final String title;
  final String hint;
  final int maxLength;
  final int minLine;
  final int maxLine;
  final bool enabled;
  final bool obscuredText;
  final TextInputAction textInputAction;
  final TextCapitalization textCapitalization;
  final bool alignLabelWithHint;
  final bool readOnly;
  final bool enableInteractiveSelection;
  final bool borda;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final VoidCallback onTap;
  final bool onlyNumber;
  final bool autofocus;

  TextFormFieldWidget({
    this.controller,
    this.initialValue,
    this.title,
    this.hint,
    this.funcValidator,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.onChanged,
    this.textInputType,
    this.focusNode,
    this.nextFocusNode,
    this.textInputFormatter,
    this.maxLength,
    this.minLine,
    this.maxLine,
    this.enabled: true,
    this.obscuredText: false,
    this.textInputAction,
    this.textCapitalization,
    this.alignLabelWithHint: false,
    this.readOnly: false,
    this.enableInteractiveSelection: true,
    this.borda: true,
    this.onTap,
    this.prefixIcon,
    this.suffixIcon,
    this.onlyNumber: false,
    this.autofocus: false,
  }) : assert((controller == null || initialValue == null)), assert(title != null || hint != null) {
    if (textInputFormatter != null) {
      _listTextInput.add(textInputFormatter);
    }
    if((onlyNumber ?? false)){
      _listTextInput.add(FilteringTextInputFormatter.allow(RegExp('[0-9]')));
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      autofocus: autofocus,
      initialValue: initialValue,
      focusNode: focusNode,
      maxLength: maxLength,
      minLines: minLine ?? 1,
      maxLines: maxLine ?? 1,
      enabled: enabled,
      readOnly: readOnly,
      obscureText: obscuredText,
      inputFormatters: _listTextInput,
      enableInteractiveSelection: enableInteractiveSelection,
      keyboardType: textInputType ?? TextInputType.text,
      decoration: _inputDecoration(title, hint, prefixIcon, alignLabelWithHint, 12, borda),
      validator: funcValidator,
      textInputAction: textInputAction ?? TextInputAction.done,
      textCapitalization: textCapitalization ?? TextCapitalization.sentences,
      onEditingComplete: onEditingComplete ?? () { FocusScope.of(context).requestFocus(nextFocusNode ?? FocusNode()); },
      onFieldSubmitted: onFieldSubmitted ?? (value) { FocusScope.of(context).requestFocus(nextFocusNode ?? FocusNode()); },
      onChanged: onChanged ?? (value) {},
      onTap: onTap ?? (){},
      style: TextStyle(
        color: ThemeHelper.instance.textTheme.caption.color,
        fontSize: 12.5,
      ),
    );
  }

  InputDecoration _inputDecoration(String label, String labelHint, Widget prefixIcon, bool alignLabelWithHint, double fontSize, bool borda) {
    if (borda) {
      return UtilStyle.inputDecoration(title, labelHint: hint, alignLabelWithHint: alignLabelWithHint, fontSize: 12, prefixIcon: prefixIcon, suffixIcon: suffixIcon);
    }
    return UtilStyle.inputDecorationSemBorda(title, labelHint: hint, alignLabelWithHint: alignLabelWithHint, fontSize: 12, prefixIcon: prefixIcon, suffixIcon: suffixIcon);
  }
}
