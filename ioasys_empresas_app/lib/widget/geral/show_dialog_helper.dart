import 'package:bot_toast/bot_toast.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showToastCustom(String msg, {int duration: 5, Alignment alignment: Alignment.center}) {
  BotToast.showText(
    text: msg,
    clickClose: false,
    align: alignment,
    duration: Duration(seconds: duration),
    textStyle: TextStyle(fontSize: 14.5, color: Colors.white),
    contentPadding: const EdgeInsets.all(8),
    onlyOne: true,
  );
}