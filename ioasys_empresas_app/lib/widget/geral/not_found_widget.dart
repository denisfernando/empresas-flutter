import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/library/theme_helper.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:ioasys_empresas_app/library/util_style.dart';

class NotFoundWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.7,
      child: Center(
        child: Text(
          UtilText.msgNotFound,
          style: UtilStyle.textStyleCustom(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: ThemeHelper.instance.colorBackgroundGrey),
        ),
      ),
    );
  }
}
