import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/bloc/home_enterprise_bloc.dart';
import 'package:ioasys_empresas_app/library/util_style.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';

class SearchFieldWidget extends StatelessWidget {
  final HomeEnterpriseBloc bloc;
  SearchFieldWidget(this.bloc);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 50,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: TextFormField(
            textInputAction: TextInputAction.done,
            textCapitalization: TextCapitalization.none,
            decoration: UtilStyle.inputDecorationSearch(UtilText.msgHintSearch),
            style: UtilStyle.textFormStyleLogin(),
            onChanged: (value) {
              search(
                bloc,
                value,
              );
            },
          ),
        )
      ],
    );
  }

  void search(HomeEnterpriseBloc bloc, String value) async {
    bloc.attResultsApi(value?.trim() ?? '');
  }
}
