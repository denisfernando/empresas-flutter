import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/bloc/home_enterprise_bloc.dart';
import 'package:ioasys_empresas_app/enum/enum_retorno.dart';
import 'package:ioasys_empresas_app/library/theme_helper.dart';
import 'package:ioasys_empresas_app/library/util_style.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:ioasys_empresas_app/model/enterprise_model.dart';
import 'package:ioasys_empresas_app/model/retorno_model.dart';
import 'package:ioasys_empresas_app/tile/enterprise_tile.dart';
import 'package:ioasys_empresas_app/widget/geral/not_found_widget.dart';
import 'package:ioasys_empresas_app/widget/geral/progress_indicator_widget.dart';

class ListEnterpriseWidget extends StatelessWidget {
  final HomeEnterpriseBloc bloc;
  ListEnterpriseWidget(this.bloc,);
  @override
  Widget build(BuildContext context) {
return Column(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.20,
        ),
        StreamBuilder<RetornoModel>(
          stream: bloc.streamEnterprises,
          builder: (_, snapshot) {
            final retornoModel = snapshot.data;
            if (retornoModel != null) {
              switch (retornoModel.state) {
                case EnumRetorno.processando:
                  return Center(
                    child: ProgressIndicatorWidget(),
                  );
                case EnumRetorno.concluido:
                case EnumRetorno.erro:
                case EnumRetorno.none:
                  if (retornoModel.data != null &&
                      retornoModel.data is List<EnterpriseModel> &&
                      retornoModel.data.isNotEmpty) {
                    final listaAux = retornoModel.data as List<EnterpriseModel>;
                    final qtd = listaAux.length;
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 6),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '$qtd ${qtd > 1 ? UtilText.msgResultadosEncontrados : UtilText.msgResultadoEncontrado}',
                            style: UtilStyle.textStyleCustom(
                              fontSize: 15,
                              color: ThemeHelper.instance.colorBackgroundGrey,
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height * 0.769,
                            child: ListView.separated(
                              itemCount: qtd,
                              itemBuilder: (_, index) {
                                return EnterpriseTile(listaAux[index], index);
                              },
                              separatorBuilder: (_, index) {
                                return Padding(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 1,
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  }
              }
            }
            return NotFoundWidget();
          },
        ),
      ],
    );
  }
}