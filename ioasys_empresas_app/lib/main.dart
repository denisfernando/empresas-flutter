
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ioasys_empresas_app/screen/home_enterprise_screen.dart';
import 'package:ioasys_empresas_app/screen/login_screen.dart';
import 'package:ioasys_empresas_app/screen/splash_screen.dart';
import 'package:ioasys_empresas_app/service/app_service.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'bloc/app_bloc.dart';
import 'enum/enum_acao_inicial_app.dart';
import 'library/theme_helper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  runApp(
    Provider<AppBloc>(
      create: (_) {
        return AppBloc();
      },
      dispose: (_, value){
        value?.dispose();
      },
    child: Consumer<AppBloc>(
        builder: (_, bloc, __){
          return MyApp(bloc);
        },
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  
  final AppBloc appBloc;
  MyApp(this.appBloc) : assert(appBloc != null);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [const Locale('pt', 'BR')],
      theme: ThemeHelper.instance.classic,
      debugShowCheckedModeBanner: false,
      builder: BotToastInit(),
      navigatorObservers: [BotToastNavigatorObserver()],
      navigatorKey: AppService.instance.navigatorKey,
      home: StreamBuilder<bool>(
        stream: appBloc.streamResumeApp,
        builder: (_, __) {
          return SplashScreen(
            onInitializationComplete: _onScreen,
            appBloc: appBloc,
          );
        }
      ),
    );
  }

  void _onScreen(EnumAcaoInicialApp acaoInicial) {
    switch (acaoInicial) {
      case EnumAcaoInicialApp.login:
        AppService.instance.navigateFromLogin(LoginScreen());
        break;
      case EnumAcaoInicialApp.home:
        AppService.instance.navigateFromLogin(HomeEnterpriseScreen());
        break;
    }
  }
}