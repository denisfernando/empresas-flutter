import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/enum/enum_acao_inicial_app.dart';
import 'package:ioasys_empresas_app/enum/enum_status_login.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:ioasys_empresas_app/library/validator.dart';
import 'package:ioasys_empresas_app/model/exception_model.dart';
import 'package:ioasys_empresas_app/model/usuario_model.dart';
import 'package:ioasys_empresas_app/screen/home_enterprise_screen.dart';
import 'package:ioasys_empresas_app/service/app_service.dart';
import 'package:ioasys_empresas_app/service/usuario_service.dart';
import 'package:ioasys_empresas_app/widget/geral/show_dialog_helper.dart';
import 'package:rxdart/rxdart.dart';

import 'app_bloc.dart';
import 'bloc_base.dart';

class LoginBloc implements Bloc {

  final double horizontalPadding = 30;
  final AppBloc appBloc;
  final UsuarioModel _usuarioModel = UsuarioModel();

  BehaviorSubject<bool> _controllerVisualizarSenha;
  Function(bool) get sinkVisualizarSenha { return _controllerVisualizarSenha.sink.add; }
  Stream<bool> get streamVisualizarSenha { return _controllerVisualizarSenha.stream; }

  BehaviorSubject<EnumStatusLogin> _controllerEnabledLogin;
  Function(EnumStatusLogin) get _sinkEnabledLogin { return _controllerEnabledLogin.sink.add; }
  Stream<EnumStatusLogin> get streamEnabledLogin { return _controllerEnabledLogin.stream; }



  LoginBloc(this.appBloc) {
    _controllerVisualizarSenha = BehaviorSubject<bool>.seeded(true);
    _controllerEnabledLogin = BehaviorSubject<EnumStatusLogin>.seeded(EnumStatusLogin.none);
  }

  void attUsuario({String email, String senha}) {
    _usuarioModel.email = email ?? _usuarioModel.email;
    _usuarioModel.password = senha ?? _usuarioModel.password;
    final validaEmail = emailValidator(_usuarioModel.email);
    final validaSenha = senhaValidator(_usuarioModel.password);
    _sinkEnabledLogin(validaEmail == null && validaSenha == null ? EnumStatusLogin.habilitado : EnumStatusLogin.none);
  }

  String emailValidator(String value) {
    if (value == null || value.trim().isEmpty) {
      return UtilText.msgEmptyEmail;
    } else if(!Validator.isValidoEmail(value)){
      return UtilText.msgValidationLogin;
    }
    return null;
  }

  String senhaValidator(String value) {
    if (value == null || value.trim().isEmpty) {
      return UtilText.msgEmptyPassword;
    } else if(!Validator.isValidoPassword(value)) {
      return UtilText.msgValidationPasswordInsufficient;
    }
    return null;
  }

  Future<void> autenticarUsuario(BuildContext context) async {
    _sinkEnabledLogin(EnumStatusLogin.processando);
    try {
      _onNextPage(await UsuarioService.instance.autenticarUsuario(_usuarioModel.email, _usuarioModel.password, true, ehAuth: true));
    } on ExceptionModel catch (e) {
      showToastCustom(e.msg ?? UtilText.msgErroAuthUser, alignment: Alignment.center);
    } catch (e) {
      showToastCustom(UtilText.msgErroAuthUser, alignment: Alignment.center);
    } finally {
      _sinkEnabledLogin(EnumStatusLogin.habilitado);
    }
  }

  void _onNextPage(bool value) async {
    if(!value) return;
    switch (await appBloc.onInitializationComplete()) {
      case EnumAcaoInicialApp.login:
        /// none
        break;
      case EnumAcaoInicialApp.home:
        AppService.instance.navigatePushAndRemoveUntil(HomeEnterpriseScreen());
        break;
    }
  }

  @override
  void dispose() async {
    await _controllerVisualizarSenha?.close();
    await _controllerEnabledLogin?.close();
  }
}