import 'package:ioasys_empresas_app/enum/enum_acao_inicial_app.dart';
import 'package:ioasys_empresas_app/enum/enum_idioma.dart';
import 'package:ioasys_empresas_app/model/usuario_model.dart';
import 'package:ioasys_empresas_app/service/app_service.dart';
import 'package:ioasys_empresas_app/service/idioma_service.dart';
import 'package:ioasys_empresas_app/service/usuario_service.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';


class AppBloc implements Bloc {

  BehaviorSubject<bool> _controllerResumeApp;
  Function(bool) get resumeApp { return _controllerResumeApp.sink.add; }
  Stream<bool> get streamResumeApp { return _controllerResumeApp.stream; }

  final _applicationService = AppService.instance;
  final _userService = UsuarioService.instance;
  final _idiomarService = IdiomaService.instance;

  AppBloc() {
    _controllerResumeApp = BehaviorSubject<bool>();
  }

  UsuarioModel get usuario { return _userService.usuario; }
  EnumIdioma get idiomaUsuario { return _idiomarService.idiomaUsuario; }

  Future<EnumAcaoInicialApp> onInitializationComplete() async {
    await _idiomarService.configIdioma();
    final existeUsuario = await _userService.attUsuario();
    if(existeUsuario) return EnumAcaoInicialApp.home;
    return EnumAcaoInicialApp.login;
  }

  Future<void> attIdioma(EnumIdioma idioma) async {
    if(idioma == null) return;
    try {
      final sucesso = await _idiomarService.salvarIdioma(idioma);
      if(sucesso) {
        await _idiomarService.configIdioma();
        resumeApp(true);
      }
    } catch(_){}
  }

  void finallySession(bool apagarDadosUsuario, bool sairDoApp) async {
    _applicationService.finallySession(apagarDadosUsuario, sairDoApp);
  }

  @override
  void dispose() async {
    await _controllerResumeApp?.close();
  }
}