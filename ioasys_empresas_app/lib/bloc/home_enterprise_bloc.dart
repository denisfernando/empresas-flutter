import 'package:ioasys_empresas_app/enum/enum_retorno.dart';
import 'package:ioasys_empresas_app/model/exception_model.dart';
import 'package:ioasys_empresas_app/model/retorno_model.dart';
import 'package:ioasys_empresas_app/service/home_enterprise_service.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class HomeEnterpriseBloc extends Bloc{
  String filterName = '';

  BehaviorSubject<RetornoModel> _controllerEnterprises;
  Function(RetornoModel) get _sinkEnterprises { return _controllerEnterprises.sink.add; }
  Stream<RetornoModel> get streamEnterprises {return _controllerEnterprises.stream; }

  HomeEnterpriseBloc(){
    _controllerEnterprises = BehaviorSubject<RetornoModel>.seeded(null);
  }

  void attResultsApi(String filter) async{
    try{
      filterName = filter?.trim() ?? '';
      _sinkEnterprises(RetornoModel(EnumRetorno.processando, _controllerEnterprises?.value?.data));

      final response = await HomeEnterpriseService.instance.getEnterprisesIndex(filterName);

      _sinkEnterprises(RetornoModel(EnumRetorno.concluido, response));
    } on ExceptionModel catch(_) {
      _sinkEnterprises(RetornoModel(EnumRetorno.erro, _controllerEnterprises?.value?.data));
    } catch (e) {
      _sinkEnterprises(RetornoModel(EnumRetorno.erro, _controllerEnterprises?.value?.data));
    }
  }
  

  @override
  void dispose() async{
    await _controllerEnterprises.close();
  }
}