
import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/bloc/app_bloc.dart';
import 'package:ioasys_empresas_app/bloc/login_bloc.dart';
import 'package:ioasys_empresas_app/widget/login/head_logo_widget.dart';
import 'package:ioasys_empresas_app/widget/login/login_button_widget.dart';
import 'package:ioasys_empresas_app/widget/login/login_form_widget.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<AppBloc>(context);
    return Scaffold(
      body: Provider<LoginBloc>(
        create: (_) {
          return LoginBloc(bloc);
        },
        dispose: (_, value) {
          return value?.dispose();
        },
        child: ListView(
          padding: const EdgeInsets.only(top: 0),
          children: <Widget>[
            HeadLogoWidget(),
            LoginFormWidget(),
            LoginButtonWidget(),
          ],
        ),
      ),
    );
  }
}
