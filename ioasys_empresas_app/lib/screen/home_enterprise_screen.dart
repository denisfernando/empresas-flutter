import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/bloc/home_enterprise_bloc.dart';
import 'package:ioasys_empresas_app/inherited/base_screen.dart';
import 'package:ioasys_empresas_app/library/theme_helper.dart';
import 'package:ioasys_empresas_app/widget/Enterprise/list_enterprise_widget.dart';
import 'package:ioasys_empresas_app/widget/Enterprise/search_field_widget.dart';
import 'package:provider/provider.dart';

class HomeEnterpriseScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<HomeEnterpriseBloc>(
      create: (_) {
        return HomeEnterpriseBloc();
      },
      dispose: (_, value) {
        value?.dispose();
      },
      child: Consumer<HomeEnterpriseBloc>(
        builder: (_, bloc, __) {
          bloc.attResultsApi('');
          return _build(context, bloc);
        },
      ),
    );
  }

  Widget _build(BuildContext context, HomeEnterpriseBloc bloc) {
    return BaseScreen(
      showAppBar: false,
      body: SingleChildScrollView(
        child: Stack(
          children: [
            _buildContainerAppBar(),
            Positioned(
              child: ListEnterpriseWidget(bloc),
            ),
            Positioned(
              child: SearchFieldWidget(bloc,),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildContainerAppBar() {
    return Container(
      height: 80,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: ThemeHelper.instance.colorsGradient,
        ),
      ),
    );
  }
}
