import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/bloc/app_bloc.dart';
import 'package:ioasys_empresas_app/enum/enum_acao_inicial_app.dart';
import 'package:ioasys_empresas_app/library/theme_helper.dart';
import 'package:ioasys_empresas_app/library/util.dart';

class SplashScreen extends StatefulWidget {
  final Function(EnumAcaoInicialApp) onInitializationComplete;
  final AppBloc appBloc;

  SplashScreen({
    @required this.onInitializationComplete,
    @required this.appBloc,
  }) : assert(onInitializationComplete != null), assert(appBloc != null);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _offsetAnimation;

  @override
  void initState() {
    super.initState();
    _initAnimated();
  }

  @override
  void dispose() {
    super.dispose();
    _controller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: <Widget>[
          _buildBackground(context),
          SlideTransition(
            position: _offsetAnimation,
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: _buildLogo(context),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBackground(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: ThemeHelper.instance.colorsGradient,
        ),
      ),
    );
  }

  Widget _buildLogo(BuildContext context) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        height: 200,
        child: Image.asset(
          Util.assetsLogoSplash,
          fit: BoxFit.fitHeight,
          alignment: Alignment.center,
          //color: ThemeHelper.instance.colorSecondary,
        ),
      ),
    );
  }

  void _onInit() async {
    final retorno = await widget.appBloc.onInitializationComplete();
    widget.onInitializationComplete(retorno);
  }

void _initAnimated() {                                                                     
    _controller = AnimationController(duration: const Duration(milliseconds: 1000), vsync: this);
    _offsetAnimation = Tween<Offset>(begin: const Offset(0.0, -1), end: Offset.zero).animate(CurvedAnimation(parent: _controller, curve: Curves.elasticIn));
    _controller?.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _controller.stop();
        _onInit();
      }
    });
    _controller.forward();
  }
}