import 'dart:io';
import 'package:dio/dio.dart';
import 'package:ioasys_empresas_app/library/constantes.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:ioasys_empresas_app/model/exception_model.dart';
import 'package:ioasys_empresas_app/service/usuario_service.dart';
import 'package:meta/meta.dart';

const int receiveTimeout = 10000;
const int connectTimeout = 10000;
const _URL_BASE = '';

class HttpClient {
  final _userService = UsuarioService.instance;

  static final HttpClient _httpClient = HttpClient._internal();
  static HttpClient get instance { return _httpClient; }
  HttpClient._internal();

  String get urlBase { return _URL_BASE; }

  Future<Response> post({@required String url, @required String data, bool useFormatData: false, List<String> listFilePath, bool exibirErroServidor: false, @required bool ehAuth}) async {
    if(data == null || data.isEmpty) return null;
    final dio = await _instanceDio(url, timeoutConnect: connectTimeout, timeoutReceive: receiveTimeout);
    try {
      Response response;
      if (!useFormatData) {
        response = await dio.post(url, data: data);
      } else {
        FormData formData = FormData();
        formData.files.add(MapEntry('dto', MultipartFile.fromString(data)));
        if (listFilePath != null && listFilePath.length > 0) {
          for (final path in listFilePath) {
            final fileAux = File(path ?? '');
            if (fileAux.existsSync()) {
              final String fileName = path.split('/').last;
              formData.files.add(
                MapEntry(
                  fileName,
                  MultipartFile.fromFileSync(fileAux.path, filename: fileName),
                ),
              );
            }
          }
        }
        response = await dio.post(url, data: formData);
      }
      if (response != null && response.data != null) return response;
    } on DioError catch (error) {
      _buildException(error, exibirErroServidor, ehAuth: ehAuth);
    }
    return null;
  }

  Future<Response> get({@required String url, bool exibirErroServidor: false, int timeoutConnect, int timeoutReceive,}) async {
    final dio = await _instanceDio(url, timeoutConnect: timeoutConnect, timeoutReceive: timeoutReceive);
    try {
      final response = await dio.get(url);
      if (response.data != null) {
        return response;
      }
    } on DioError catch (error) {
      _buildException(error, exibirErroServidor);
    }
    return null;
  }

  Future<Dio> _instanceDio(String url, {int timeoutConnect, int timeoutReceive,}) async {
    BaseOptions options = new BaseOptions(
      connectTimeout: timeoutConnect ?? connectTimeout,
      receiveTimeout: timeoutReceive ?? receiveTimeout,
    );
    final dio = Dio(options);
    final urlAuthUser = _userService.urlAuth();
    if (url.compareTo(urlAuthUser) != 0) {
      dio.interceptors.add(InterceptorsWrapper(
        onRequest: (RequestOptions options) {
          options.headers['access-token'] = _userService?.usuario?.accessToken ?? '';
          options.headers['client'] = _userService?.usuario?.client ?? '';
          options.headers['uid'] = _userService?.usuario?.uid ?? '';
          return options;
        },
      ));
    }
    return dio;
  }

  void _buildException(DioError error, bool exibirErroServidor, { bool ehAuth = false}) {
    final statusCode = error?.response?.statusCode ?? ERRO_SERVER;
    // erro de conexão (sem internet ou dados)
    if (error.type == DioErrorType.DEFAULT) {
      throw ExceptionModel(codigo: 0, msg: !exibirErroServidor ? 'Connection error' : error?.response?.data ?? 'Connection error');
    } else if (error.type == DioErrorType.CONNECT_TIMEOUT || error.type == DioErrorType.RECEIVE_TIMEOUT) {
      throw ExceptionModel(codigo: ERRO_TIMEOUT_SERVER, msg: 'Error timeout');
    } else if (statusCode == ERRO_TOKEN_VENCIDO && !ehAuth) {
      throw ExceptionModel(codigo: ERRO_TOKEN_VENCIDO, msg: !exibirErroServidor ? UtilText.msgErroToken : error?.response?.data?.toString());
    } else if(statusCode == ERRO_USUARIO_SENHA && ehAuth) {
      throw ExceptionModel(codigo: ERRO_USUARIO_SENHA, msg: !exibirErroServidor ? UtilText.msgErroAuthUser : error?.response?.data?.toString());
    } else if (statusCode == ERRO_NOT_FOUND) {
      throw ExceptionModel(codigo: ERRO_NOT_FOUND, msg: 'Server not found');
    } else if (statusCode == ERRO_SERVER || statusCode > 226) {
      throw ExceptionModel(codigo: ERRO_SERVER, msg: !exibirErroServidor ? 'Error server' : error?.response?.data?.toString());
    }
  }
}
