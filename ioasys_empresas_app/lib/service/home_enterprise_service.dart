
import 'package:ioasys_empresas_app/model/enterprise_model.dart';
import 'package:ioasys_empresas_app/model/exception_model.dart';
import 'package:ioasys_empresas_app/service/service_base.dart';

class HomeEnterpriseService extends ServiceBase {
  static final HomeEnterpriseService _service = HomeEnterpriseService._internal();
  HomeEnterpriseService._internal();
  static HomeEnterpriseService get instance { return _service; }

  @override
  String urlAPI() { return join(urlBaseApiVersao, 'enterprises'); }
  String get urlGetEnterprisesIndex { 
    final url = urlAPI();
    return "$url?enterprise_types=2&name=";
   }

  Future<List<EnterpriseModel>> getEnterprisesIndex(String filter) async{
    try{
      final url = '$urlGetEnterprisesIndex${filter ?? ''}';
      final response = await get(url: url);
      if(response != null && response.data != null){
        final lista = response.data['enterprises']?.map<EnterpriseModel>((value){ return EnterpriseModel.fromMap(value); })?.toList();
        return lista;
      }
    } on ExceptionModel catch(e) {
      print(e.toMap());
    } catch(e) {
      print(e);
    }
    return null;
  }
}