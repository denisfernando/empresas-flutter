
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:ioasys_empresas_app/library/constantes.dart';
import 'package:ioasys_empresas_app/library/util.dart';
import 'package:ioasys_empresas_app/model/exception_model.dart';
import 'package:ioasys_empresas_app/service/http_client.dart';
import 'package:ioasys_empresas_app/service/usuario_service.dart';

import 'app_service.dart';
export 'package:path/path.dart';

abstract class ServiceBase {
  String get urlBaseApiVersao { return "${Util.urlBase}/${Util.api}/${Util.versaoApi}"; }

  Future<Response> get({@required String url, bool exibirErroServidor: false, bool isForcarObterToken: true, int timeoutConnect, int timeoutReceive}) async {
    try {
      return await HttpClient.instance.get(url: url, exibirErroServidor: exibirErroServidor, timeoutConnect: timeoutConnect, timeoutReceive: timeoutReceive);
    } on ExceptionModel catch (e){
      if (e.codigo == ERRO_TOKEN_VENCIDO && isForcarObterToken) {
        if (await forcarObterToken()) {
          return await get(url: url, exibirErroServidor: exibirErroServidor, isForcarObterToken: false);
        }
      }
      throw ExceptionModel(codigo: e.codigo, msg: _normalizarMsg(e.msg));
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<Response> post({@required String url, @required String data, bool useFormatData: false, List<String> listFilePath, bool exibirErroServidor: false, bool isForcarObterToken: true, bool ehAuth, }) async {
    try {
      return await HttpClient.instance.post(url: url, data: data, useFormatData: useFormatData, listFilePath: listFilePath, exibirErroServidor: exibirErroServidor, ehAuth: ehAuth);
    } on ExceptionModel catch (e){
      if(e.codigo == ERRO_USUARIO_SENHA && !ehAuth){
        isForcarObterToken = false;
        AppService.instance.finallySession(true, false);
        return null;
        ///TODO o isForcarObterToken serve para o caso em que o Token expirou, e a senha do usuário foi trocada fora do APP
        ///Como o código retorno para ambos acontecimentos é o 401, sem essa variável de controle entraria em LOOP infinito tentando 
        ///Atualizar o access-token, client e uid, e com ela ele faz uma tentativa, se retornou 401 novamente, joga pra tela de login
      } else if (e.codigo == ERRO_TOKEN_VENCIDO && isForcarObterToken) { 
        if (await forcarObterToken()) {
          return await post(url: url, data: data, useFormatData: useFormatData, listFilePath: listFilePath, exibirErroServidor: exibirErroServidor, isForcarObterToken: false);
        }
      }
      throw ExceptionModel(codigo: e.codigo, msg: _normalizarMsg(e.msg));
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<bool> forcarObterToken() async {
    try {
      final user = UsuarioService.instance.usuario;
      if (UsuarioService.instance.isUsuarioValido(user)) {
        final retorno = await UsuarioService.instance.autenticarUsuario(user.email, user.password, true);
        return retorno;
      }
    } catch (e) {}
    return false;
  }

  String _normalizarMsg(String msg) {
    if(msg == null || msg.isEmpty) return msg;
    return msg.replaceAll('"', '').replaceAll('<br/>', '').replaceAll('<br />', '').replaceAll('<br', '').replaceAll('>', '').replaceAll(';', ' ').trim();
  }

  String urlAPI();
}