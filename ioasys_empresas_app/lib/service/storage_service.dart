import 'package:shared_preferences/shared_preferences.dart';

class StorageService {
  static final StorageService _service = StorageService._internal();
  static StorageService get instance { return _service; }
  StorageService._internal();

  Future<bool> saveData(String nameKey, String value, ) async {
    if((nameKey == null || nameKey.isEmpty) || (value == null || value.isEmpty)) return false;
    
    return _saveDataShared(nameKey, value);
  }

  Future<dynamic> loadData(String nameKey) async {
    return await _loadDataShared(nameKey);
  }

  Future<bool> deleteData(String nameKey) async {
    return await _deleteDataShared(nameKey);
  }

  Future<void> clear() async {
    await _clearShared();
  }


  /// Rotinas do Shared Preferences
  Future<bool> _saveDataShared(String nameKey, String value) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return await prefs.setString(nameKey, value);
    } catch (e) { print(e); }
    return false;
  }

  Future<String> _loadDataShared(String nameKey) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return prefs.getString(nameKey);
    } catch(e){ print(e); }
    return null;
  }

  Future<bool> _deleteDataShared(String nameKey) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return await prefs.remove(nameKey);
    } catch (e) {
      print(e);
    }
    return false;
  }

  Future<bool> _clearShared() async {
    try{
      final prefs = await SharedPreferences.getInstance();
      return prefs.clear();
    } catch(e){}
    return false;
  }
}