
import 'package:ioasys_empresas_app/enum/enum_idioma.dart';
import 'package:ioasys_empresas_app/library/enum_helper.dart';
import 'package:ioasys_empresas_app/library/format_text.dart';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:ioasys_empresas_app/model/idioma_model.dart';
import 'package:ioasys_empresas_app/service/storage_service.dart';

const _KEY_LANGUAGE = 'key_language';

class IdiomaService {
  EnumIdioma _idiomaUser = EnumIdioma.portugues;

  static final IdiomaService _languageService = IdiomaService._internal();
  IdiomaService._internal();
  static IdiomaService get instance { return _languageService; }

  void _attIdioma(EnumIdioma idioma) {
    _idiomaUser = idioma;
  }

  Future<bool> configIdioma() async {
    try {
      final strIdioma = await _loadSharedIdioma();
      if (strIdioma != null && strIdioma.isNotEmpty){
        final retorno = enumFromString<EnumIdioma>(EnumIdioma.values, strIdioma);
        _attIdioma(retorno ?? _idiomaUser);
      }
      return true;
    } catch(_){}
    return false;
  }

  EnumIdioma get idiomaUsuario { return _idiomaUser; }

  List<IdiomaModel> getListIdioma() {
    return List<IdiomaModel>()
    ..add(_getIdiomaModel(EnumIdioma.portugues, UtilText.idiomaPortugues))
    ..add(_getIdiomaModel(EnumIdioma.ingles, UtilText.idiomaIngles))
    ..add(_getIdiomaModel(EnumIdioma.espanhol, UtilText.idiomaEspanhol))
    ..add(_getIdiomaModel(EnumIdioma.frances, UtilText.idiomaFrances));
  }

  IdiomaModel _getIdiomaModel(EnumIdioma enumIdioma, String nome) {
    return IdiomaModel(id: enumIdioma, nome: FormatText.capitalizarPalavra(nome));
  }

  /// Shared Preferences
  Future<String> _loadSharedIdioma() async {
    return await StorageService.instance.loadData(_KEY_LANGUAGE,);
  }

  Future<bool> salvarIdioma(EnumIdioma idioma) async {
    if(idioma == null) return false;
    _attIdioma(idioma);
    return await _saveSharedIdioma(idioma.toString());
  }

  Future<bool> _saveSharedIdioma(String idioma) async {
    return await StorageService.instance.saveData(_KEY_LANGUAGE, idioma,);
  }
}
