import 'dart:convert';
import 'package:ioasys_empresas_app/library/util_text.dart';
import 'package:ioasys_empresas_app/model/exception_model.dart';
import 'package:ioasys_empresas_app/model/usuario_model.dart';
import 'package:ioasys_empresas_app/service/service_base.dart';
import 'package:ioasys_empresas_app/service/storage_service.dart';

const _KEY_USER = 'key_user_';

class UsuarioService extends ServiceBase {
  static final UsuarioService _service = UsuarioService._internal();
  UsuarioService._internal();
  static UsuarioService get instance { return _service; }

  @override
  String urlAPI() { return join(urlBaseApiVersao, 'users'); }
  String urlAuth() { return join(urlAPI(), 'auth', 'sign_in'); }

  UsuarioModel _usuarioModel;

  UsuarioModel get usuario { return _usuarioModel; }

  Future<bool> autenticarUsuario(String email, String password, bool forcarObterAPI, { bool ehAuth = false }) async {
    if(!_isInformacaoAuthValida(email, password)) return false;
    try {
      return await _autenticaUsuarioFactory(email, password, forcarObterAPI, ehAuth: ehAuth);
    } on ExceptionModel catch (e) {
      throw e;
    } catch(e) {
      throw ExceptionModel(codigo: 0, msg: UtilText.msgErroAuthUser);
    }
  }

  Future<bool> _autenticaUsuarioFactory(String email, String password, bool forcarObterAPI, { bool ehAuth = false }) async {
    bool autenticou = false;
    try {
      /// Caso seja pra buscar localmente e nao encontre, entao busca na API
      if(!forcarObterAPI) autenticou = (await _loadUsuario(UsuarioModel(email: email, password: password))) != null;
      if(autenticou) return autenticou;
      final usuarioAuthAux = await _generateAuthUsuario(email, password);
      if(usuarioAuthAux == null) return autenticou;
      final accessKeys = await _getTokenAPI(usuarioAuthAux, ehAuth); 
      if(accessKeys == null || accessKeys.isEmpty) return autenticou;
      usuarioAuthAux.accessToken = accessKeys['access-token']?.first ?? '';
      usuarioAuthAux.uid = accessKeys['uid']?.first ?? '';
      usuarioAuthAux.client = accessKeys['client']?.first ?? '';
      autenticou = await attUsuario(usuarioAuthAux);
    } on ExceptionModel catch (e) {
      throw e;
    } catch(e) {
      throw ExceptionModel(codigo: 0, msg: UtilText.msgErroAuthUser);
    }
    return autenticou;
  }

  Future<Map> _getTokenAPI(UsuarioModel usuarioModel, bool ehAuth) async {
    try {
      final jsonStr = jsonEncode(usuarioModel.toMapAuth());
      final url = urlAuth();
      final response = await post(url: url, data: jsonStr, ehAuth: ehAuth);

      if(response.headers != null)
        return {
          'uid': response.headers['uid'], 
          'client': response.headers['client'], 
          'access-token': response.headers['access-token'],
        };
    } on ExceptionModel catch (e){
      throw e;
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<bool> attUsuario([UsuarioModel usuarioModel]) async {
    try {
      if (isUsuarioValido(usuarioModel)) {
        _usuarioModel = await _saveUsuario(usuarioModel);
      } else {
        _usuarioModel = await _loadUsuario();
      }
      return _usuarioModel != null;
    } catch(e){
      print(e);
    }
    return false;
  }

  /// HELPERS
  bool isUsuarioValido(UsuarioModel usuarioModel) {
    return (usuarioModel?.accessToken?.isNotEmpty ?? false) 
      && (usuarioModel?.uid?.isNotEmpty ?? false)
      && (usuarioModel?.client?.isNotEmpty ?? false);
  }

  bool _isInformacaoAuthValida(String email, String password) {
    return (email != null && email.isNotEmpty) && (password != null && password.isNotEmpty);
  }

  bool equalsUsuario(UsuarioModel a, UsuarioModel b) {
    if(!isUsuarioValido(a) || !isUsuarioValido(b)) return false;
    if(a.email.compareTo(b.email) != 0) return false;
    if(a.email.compareTo(b.email) != 0) return false;
    return true;
  }

  Future<UsuarioModel> _generateAuthUsuario(String email, String password) async {
    return UsuarioModel(email: email, password: password);
  }

  /// STORAGE
  Future<UsuarioModel> _saveUsuario(UsuarioModel usuarioModel) async {
    if(!isUsuarioValido(usuarioModel)) return null;
    try {
      await StorageService.instance.saveData(_KEY_USER, jsonEncode(usuarioModel.toMap()),);
      return usuarioModel;
    } catch(_){
      print(_);
    }
    return null;
  }

  Future deleteDadosUsuario() async{
    try{
      await StorageService.instance.deleteData(_KEY_USER);
    } catch(_){
      print(_);
    }
    return null;
  }

  Future<UsuarioModel> _loadUsuario([UsuarioModel compareUsuario]) async {
    try {
      final userStr = await StorageService.instance.loadData(_KEY_USER,);
      if(userStr == null || userStr.isEmpty) return null;
      final usuarioRetorno = UsuarioModel.fromMap(jsonDecode(userStr));
      /// Caso passe um usuario valido, eh pq deseja trazer o usuario e compara-lo
      if(isUsuarioValido(compareUsuario)) {
        return equalsUsuario(usuarioRetorno, compareUsuario) ? usuarioRetorno : null;
      }
      return usuarioRetorno;
    } catch(_){}
    return null;
  }
}