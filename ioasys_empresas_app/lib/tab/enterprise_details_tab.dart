import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/inherited/base_screen.dart';
import 'package:ioasys_empresas_app/library/util_style.dart';
import 'package:ioasys_empresas_app/model/enterprise_model.dart';
import 'package:ioasys_empresas_app/tile/enterprise_tile.dart';

class EnterpriseDetailsTab extends StatelessWidget {
  final EnterpriseModel model;
  final int index;
  EnterpriseDetailsTab(
    this.model,
    this.index,
  );

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: Text(
        model.enterpriseName,
        style: UtilStyle.textStyleCustom(
          fontSize: 15,
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),
      ),
      colorAppBar: Colors.white,
      body: Column(
        children: [
          EnterpriseTile(model, index),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
              child: Text(
                model.description,
                style: UtilStyle.textStyleCustom(
                  fontSize: 16,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
