import 'base_model.dart';

class UsuarioModel implements Model {
  String email;
  String password;
  String uid;
  String client;
  String accessToken;

  UsuarioModel({
    this.email,
    this.password,
    this.uid,
    this.client,
    this.accessToken,
  });

  @override
  Map<String, dynamic> toMap(){
    return {
      'email': email,
      'password': password,
      'uid': uid,
      'client': client,
      'access-token': accessToken,
    };
  }

  Map<String, dynamic> toMapAuth() {
    return {
      'email': email,
      'password': password,
    };
  }

  factory UsuarioModel.fromMap(Map<String, dynamic> map) {
    return UsuarioModel(
      email: map['email'],
      password: map['password'],
      uid: map['uid'],
      client: map['client'],
      accessToken: map['access-token'],
    );
  }

  @override
  UsuarioModel toCopy() {
    return UsuarioModel.fromMap(toMap());
  }
}