import 'base_model.dart';

class EnterpriseTypeModel extends Model{
    EnterpriseTypeModel({
        this.id,
        this.enterpriseTypeName,
    });

    int id;
    String enterpriseTypeName;

    factory EnterpriseTypeModel.fromMap(Map<String, dynamic> map) { 
      return EnterpriseTypeModel(
        id: map['id'],
        enterpriseTypeName: map['enterprise_type_name'],
      );
    }

    Map<String, dynamic> toMap() { 
      return {
        'id': id,
        'enterprise_type_name': enterpriseTypeName,
      };
    }

  @override
  toCopy() {
    return EnterpriseTypeModel.fromMap(this.toMap());
  }


}
