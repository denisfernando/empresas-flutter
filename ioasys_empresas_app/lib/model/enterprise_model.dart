import 'enterprise_type_model.dart';

class EnterpriseModel {
    EnterpriseModel({
        this.id,
        this.enterpriseName,
        this.description,
        this.emailEnterprise,
        this.facebook,
        this.twitter,
        this.linkedin,
        this.phone,
        this.ownEnterprise,
        this.photo,
        this.value,
        this.sharePrice,
        this.city,
        this.country,
        this.enterpriseType,
    });

    int id;
    String enterpriseName;
    String description;
    String emailEnterprise;
    String facebook;
    String twitter;
    String linkedin;
    String phone;
    bool ownEnterprise;
    String photo;
    int value;
    double sharePrice;
    String city;
    String country;
    EnterpriseTypeModel enterpriseType;

    factory EnterpriseModel.fromMap(Map<String, dynamic> map) {
      return EnterpriseModel(
          id: map['id'],
          enterpriseName: map['enterprise_name'],
          description: map['description'],
          emailEnterprise: map['email_enterprise'],
          facebook: map['facebook'],
          twitter: map['twitter'],
          linkedin: map['linkedin'],
          phone: map['phone'],
          ownEnterprise: map['own_enterprise'],
          photo: map['photo'],
          value: map['value'],
          sharePrice: map['share_price'],
          city: map['city'],
          country: map['country'],
          enterpriseType: EnterpriseTypeModel.fromMap(map['enterprise_type']),
      );
    }
    Map<String, dynamic> tomap() {
      return {
          'id': id,
          'enterprise_name': enterpriseName,
          'description': description,
          'email_enterprise': emailEnterprise,
          'facebook': facebook,
          'twitter': twitter,
          'linkedin': linkedin,
          'phone': phone,
          'own_enterprise': ownEnterprise,
          'photo': photo,
          'value': value,
          'share_price': sharePrice,
          'city': city,
          'country': country,
          'enterprise_type': enterpriseType.toMap(),
      };
    }
}