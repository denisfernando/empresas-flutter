import 'package:ioasys_empresas_app/enum/enum_retorno.dart';

class RetornoModel {
  EnumRetorno state;
  dynamic data;
  
  RetornoModel(this.state, this.data);
}