import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/enum/enum_idioma.dart';

import 'base_model.dart';

class IdiomaModel implements Model {
  EnumIdioma id;
  String nome;

  IdiomaModel({
    @required this.id,
    @required this.nome,
  }) : assert(id != null),
       assert(nome != null && nome.trim().isNotEmpty);

  factory IdiomaModel.fromMap(Map<String, dynamic> map) {
    return IdiomaModel(
      id: map['id'],
      nome: map['nome'],
    );
  }

  @override
  Map<String, dynamic> toMap(){
    return {
      'id': id,
      'nome': nome
    };
  }

  @override
  IdiomaModel toCopy() {
    return IdiomaModel.fromMap(toMap());
  }
}