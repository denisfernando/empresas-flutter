import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/library/theme_helper.dart';

class UtilStyle {
  static Color get colorPrincipal { return ThemeHelper.instance.colorPrincipal; }
  
  /// LOGIN
  static TextStyle labelTextStyleLogin() {
    return TextStyle(
      color: ThemeHelper.instance.textTheme.headline6.color,
      fontWeight: FontWeight.w500,
      fontSize: 14,
    );
  }

  static TextStyle textFormStyleLogin() {
    return TextStyle(
      color: ThemeHelper.instance.textTheme.caption.color,
    );
  }

  static InputBorder borderLogin() {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: ThemeHelper.instance.themeData.accentColor,
        style: BorderStyle.none,
      ),
    );
  }

  static InputBorder focusBorderLogin() {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: ThemeHelper.instance.themeData.accentColor,
        style: BorderStyle.none,
      ),
    );
  }

  static InputBorder enabledBorderLogin() {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: ThemeHelper.instance.textTheme.bodyText2.color,
        style: BorderStyle.none,
      ),
    );
  }

  static InputDecoration inputDecorationLogin(String title, Widget suffixIcon) {
    return InputDecoration(
      enabledBorder: UtilStyle.enabledBorderLogin(),
      focusedBorder: UtilStyle.focusBorderLogin(),
      border: UtilStyle.borderLogin(),
      labelText: title,
      labelStyle: UtilStyle.labelTextStyleLogin(),
      suffixIcon: suffixIcon,
      fillColor: Colors.grey.shade200,
      filled: true,
    );
  }

  static InputDecoration inputDecorationSearch(String hintText) {
    return InputDecoration(
      enabledBorder: UtilStyle.enabledBorderLogin(),
      focusedBorder: UtilStyle.focusBorderLogin(),
      border: UtilStyle.borderLogin(),
      hintText: hintText,
      labelStyle: UtilStyle.labelTextStyleLogin(),
      fillColor: Colors.grey.shade200,
      filled: true,
      prefixIcon: Icon(Icons.search, color: Colors.grey.shade400,),
    );
  }

  /// GERAL
  static InputDecoration inputDecoration(String label, {String labelHint, Widget prefixIcon, Widget suffixIcon, bool alignLabelWithHint: false, double fontSize: 12}) {
    return InputDecoration(
      labelText: label,
      counterText: '',
      hintText: labelHint,
      alignLabelWithHint: alignLabelWithHint,
      filled: true,
      fillColor: Colors.grey.shade200,
      labelStyle: TextStyle(color: ThemeHelper.instance.textTheme.caption.color, fontSize: fontSize),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: ThemeHelper.instance.themeData.disabledColor),
        borderRadius: BorderRadius.circular(2),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: ThemeHelper.instance.textTheme.bodyText1.color),
        borderRadius: BorderRadius.circular(2),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: ThemeHelper.instance.colorPrincipal),
        borderRadius: BorderRadius.circular(2),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: ThemeHelper.instance.themeData.errorColor),
        borderRadius: BorderRadius.circular(2),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: ThemeHelper.instance.themeData.errorColor),
        borderRadius: BorderRadius.circular(2),
      ),
      errorStyle: TextStyle(color: Colors.transparent, fontSize: 0),
      prefixIcon: prefixIcon,
      suffixIcon: suffixIcon,
      suffixStyle: TextStyle(
        color: Colors.red,
      ),
      
    );
  }

  static InputDecoration inputDecorationSemBorda(String label, {String labelHint, Widget prefixIcon, Widget suffixIcon, bool alignLabelWithHint: false, double fontSize: 16}) {
    return InputDecoration(
      labelText: label,
      counterText: '',
      hintText: labelHint,
      alignLabelWithHint: alignLabelWithHint,
      filled: true,
      fillColor: ThemeHelper.instance.themeData.disabledColor.withOpacity(0.25),
      labelStyle: TextStyle(color: ThemeHelper.instance.textTheme.caption.color, fontSize: fontSize),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          style: BorderStyle.none,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          style: BorderStyle.none,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      errorStyle: TextStyle(color: Colors.transparent, fontSize: 0),
      prefixIcon: prefixIcon,
      suffixIcon: suffixIcon,      
    );
  }

  static TextStyle textStyleCustom({@required double fontSize, FontWeight fontWeight, Color color, FontStyle fontStyle}){
    return TextStyle(
      color: color,
      fontWeight: fontWeight ?? FontWeight.w400,
      fontSize: fontSize,
      fontStyle: fontStyle ?? FontStyle.normal, 
    );
  }

  static TextStyle textStyleDialogDefault([double tamanhoFonte = 15]){
    return TextStyle(
      color: ThemeHelper.instance.textTheme.caption.color,
      fontSize: tamanhoFonte,
      fontWeight: FontWeight.w500,
    );
  }

  static TextStyle textStylePacItemHead({Color color, FontWeight fontWeight: FontWeight.w400, double height: 1}) {
    return TextStyle(
      color: color ?? ThemeHelper.instance.textTheme.overline.color, 
      fontSize: 12,
      height: height,
      fontWeight: fontWeight,
    );
  }

  static BorderRadius borderRadiusBackground() {
    return BorderRadius.vertical(
      top: Radius.circular(12),
      bottom: Radius.zero,
    );
  }

  static BoxDecoration boxDecorationContainerInputText(bool exibirBorda, {Color colorContainer, Color colorBorder}) {
    return BoxDecoration(
      color: colorContainer ?? ThemeHelper.instance.themeData.cardColor,
      borderRadius: BorderRadius.circular(10),
      border: Border.all(
        style: exibirBorda ? BorderStyle.solid : BorderStyle.none,
        color: colorBorder ?? ThemeHelper.instance.themeData.disabledColor,
      ),
    );
  }
}