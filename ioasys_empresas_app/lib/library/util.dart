import 'package:path/path.dart';

class Util {
  /// ASSETS
  static String get assetsLogoSplash { return join('assets', 'img', 'logo_splash.png'); }
  static String get assetsLogo { return join('assets', 'img', 'logo.png'); }

  static String get urlBase { return "https://empresas.ioasys.com.br"; }
  static String get api { return "api"; }
  static String get versaoApi { return "v1"; }

}