
import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/service/app_service.dart';

class ThemeHelper { 

  static final ThemeHelper _aparenciaHelper = ThemeHelper._internal();
  static ThemeHelper get instance { return _aparenciaHelper; }
  ThemeHelper._internal();

  Color get colorPrincipal { return Color.fromRGBO(224, 30, 105, 1); } /// Rosa
  Color get colorSecondary { return Color.fromRGBO(131, 39, 186, 1); } ///Roxo
  List<Color> get colorsGradient {
    return [
      ThemeHelper.instance.colorSecondary, 
      Color.fromRGBO(178, 28, 144, 1),
      ThemeHelper.instance.colorPrincipal, 
      Color.fromRGBO(229, 146, 187, 1),
    ];
  }

  Color get colorWarning { return Colors.orange.shade600; }
  Color get colorSuccess { return Colors.green; }
  Color get colorFalha { return Colors.redAccent; }
  Color get colorBlueGrey { return Colors.blueGrey; }
  Color get colorBackgroundGrey { return Colors.grey.shade500; }

  TextTheme get textTheme {
    return Theme.of(AppService.instance.context).textTheme;
  }

  ThemeData get themeData {
    return classic;
  }

  ThemeData get classic {
    final disabledColor = Colors.grey.shade600;
    return ThemeData(
      primaryColor: Colors.white,
      backgroundColor: Colors.white,
      indicatorColor: colorPrincipal,
      cursorColor: colorPrincipal,
      scaffoldBackgroundColor: Colors.white,
      disabledColor: disabledColor,
      cardColor: Colors.white,
      dividerColor: Colors.black26,
      dialogTheme: DialogTheme(
        backgroundColor: Colors.white,
      ),
      unselectedWidgetColor: Colors.white,
      textTheme: TextTheme(
        caption: TextStyle(
          color: Colors.black87,
        ),
        bodyText2: TextStyle(
          color: Colors.black87,
        ),
        headline6: TextStyle(
          color: Colors.black54,
        ),
        overline: TextStyle(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: colorSecondary,
      ),
      buttonTheme: ButtonThemeData(
        height: 44,
        buttonColor: colorPrincipal,
        disabledColor: disabledColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      popupMenuTheme: PopupMenuThemeData(
        color: Colors.white,
        textStyle: TextStyle(
          color: Colors.black87,
          fontSize: 13,
        )
      ),
      buttonColor: colorPrincipal,
      appBarTheme: AppBarTheme(
        iconTheme: IconThemeData(color: colorSecondary),
        color: colorPrincipal,
        brightness: Brightness.dark,
        elevation: 0,
        textTheme: TextTheme(
          headline6: TextStyle(
            color: colorSecondary,
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
        )
      ),
    );
  }
}