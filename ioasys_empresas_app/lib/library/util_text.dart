
import 'package:ioasys_empresas_app/enum/enum_idioma.dart';
import 'package:ioasys_empresas_app/service/idioma_service.dart';

class UtilText {
  /// @Config All
  static EnumIdioma get _idiomaUsuario { return IdiomaService.instance.idiomaUsuario; }

  /// Geral
  static String get labelButtonConfirm {
    return _getTextInternacionalizado('Confirmar', 'OK', 'OK', 'OK');
  }
  static String get labelButtonOK {
    return _getTextInternacionalizado('OK');
  }
  static String get labelButtonCancel {
    return _getTextInternacionalizado('Cancelar', 'Cancel', 'Cancelar', 'Annuler');
  }
  static String get labelProcesso {
    return _getTextInternacionalizado('Processando...', 'Cancel', 'Cancelar', 'Annuler');
  }

  static String get msgResultadosEncontrados{ 
    return _getTextInternacionalizado('Resultados encontrados', 'Results found', 'Resultados encontrados', 'Résultats trouvés'); 
  }
  static String get msgResultadoEncontrado{ 
    return _getTextInternacionalizado('Resultado encontrado', 'Result found', 'Resultado encontrado', 'Résultat trouvé'); 
  }

  /// BodyMsgWidgets
  static String get msgNotFound {
    return _getTextInternacionalizado(
      'Nenhum resultado encontrado', 'No results found',
      'Ningún resultado encontrado', 'Aucun résultat trouvé'
    );
  }
  static String get msgHintSearch {
    return _getTextInternacionalizado(
      'Pesquise por empresa', 'Search by enterprise',
      'Buscar por empresa', 'Recherche par entreprise'
    );
  }

  /// @Config Widget Idioma
  static String get labelTitleIdioma {
    return _getTextInternacionalizado('Definir idioma', 'Define language', 'Elegir lenguaje', 'Définir la langue');
  }
  static String get labelDropDownTitle {
    return _getTextInternacionalizado('Selecionar idioma', 'Select language', 'Seleccione el idioma', 'Choisir la langue');
  }
  static String get idiomaPortugues {
    return _getTextInternacionalizado('Português', 'Portuguese', 'Portugués', 'Portugais');
  }
  static String get idiomaIngles {
    return _getTextInternacionalizado('Inglês', 'English', 'Inglés', 'Anglais');
  }
  static String get idiomaEspanhol {
    return _getTextInternacionalizado('Espanhol', 'Spanish', 'Español', 'Espagnol');
  }
  static String get idiomaFrances {
    return _getTextInternacionalizado('Francês', 'French', 'Francés', 'Français');
  }
  static String get labelMudandoIdioma {
    return _getTextInternacionalizado('Alterando o idioma...', 'Applying language changes...',  'Cambiar el idioma...', 'Changement de langue...');
  }


  /// @Config Message HTTP
  static String get msgErroAuthUser {
    return _getTextInternacionalizado(
      'Credenciais incorretas', 'invalid credentials', 
      'credenciales no válidas', 'les informations d\'identification invalides',
    );
  }
  static String get msgErroToken {
    return _getTextInternacionalizado('Token inválido', 'Invalid token', 'Token no valido', 'Token invalide');
  }


  /// @Config Login
  static String get labelLogin { return 'Email'; }
  static String get labelPassword {
    return _getTextInternacionalizado('Senha', 'Password', 'Contraseña', 'Mot de passe');
  }
  static String get labelButtonAccess {
    return _getTextInternacionalizado('Entrar', 'Sign in', 'Registrarse', 'Se connecter');
  }
  static String get msgWellcome {
    return _getTextInternacionalizado('Seja bem vindo ao empresas!', 'Welcome to Enterprises!', '¡Bienvenidos a las empresas!', 'Bienvenue aux entreprises!');
  }
  static String get msgEmptyEmail {
    return _getTextInternacionalizado('Informe o login', 'Enter the login', 'Ingrese el login', 'Dire à l\'utilisateur');
  }
  static String get msgValidationLogin {
    return _getTextInternacionalizado(
      'Informe um email válido', 'Enter a valid email', 
      'Introduzca un correo electrónico válido', 'Entrer une adresse email valide',
    );
  }
  static String get msgEmptyPassword {
    return _getTextInternacionalizado('Informe a senha', 'Enter the password', 'Introduzca la contraseña', 'Entrez le mot de passe');
  }
  static String get msgValidationPasswordInsufficient {
    return _getTextInternacionalizado(
      'A senha deve conter pelo menos 6 caracteres', 'The password must contain at least 6 characters', 
      'La contraseña debe contener al menos 6 caracteres', 'Le mot de passe doit contenir au moins 6 caractères',
    );
  }

  static String get exceptionErroUsuario {
    return _getTextInternacionalizado('Erro de usuário e senha', 'Username and password error', 'Error de nombre de usuario y contraseña', 'Erreur de nom d\'utilisateur et de mot de passe');
  }

  static String _getTextInternacionalizado(String portugues, [String ingles, String espanhol, String frances]) {
    assert(portugues != null && portugues.isNotEmpty);
    switch (_idiomaUsuario) {
      case EnumIdioma.ingles:
        return ingles ?? portugues;
        break;
      case EnumIdioma.espanhol:
        return espanhol ?? portugues;
        break;
      case EnumIdioma.frances:
        return frances ?? portugues;
        break;
      default:
        return portugues;
    }
  }
}