import 'package:intl/intl.dart';

class FormatNumber {
  //static final NumberFormat _formatter = NumberFormat.decimalPattern('pt_BR');
  static String formatDouble(double value){
    final formatter = NumberFormat('#,##0.00', 'pt_BR');
    try{
      return formatter.format(value);
    } catch (e) {
      print(e);
    }
    return '0';
  }

  static String formatInteger(num value){
    final formatter = NumberFormat('#,##0', 'pt_BR');
    try{
      return formatter.format(value);
    } catch (e) {
      print(e);
    }
    return '0';
  }

  static String formatCurrency(double value){
    final formatter = NumberFormat.simpleCurrency(locale: 'pt_BR');
    try{
      return formatter.format(value);
    } catch (e) {
      print(e);
    }
    return '0';
  }

  static String numberToString({double valueDouble, int valueInt, bool vazioEhZero: false}){
    if (valueDouble != null){
      return valueDouble.toString();
    } else if(valueInt != null) {
      return valueInt.toString();
    }
    return vazioEhZero ? '0' : '';
  }

  static double stringToDouble(String text) {
    try {
      if (text != null && text.isNotEmpty){
        return double.parse(text);
      }
    } catch(e) {}
    return 0;
  }

  static double normalizarStringToDouble(String textDouble){
    try {
      textDouble = textDouble.replaceAll('.', '');
      textDouble = textDouble.replaceAll(',', '.');

      return double.parse(textDouble);
    } catch(e) {}
    return 0;
  }
}