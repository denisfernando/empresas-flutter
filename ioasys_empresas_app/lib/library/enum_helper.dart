T enumFromString<T>(Iterable<T> list, String value) {
  if(list == null || value == null || value.isEmpty) return null;
  return list.firstWhere((type) {
    final typeStr = _normalizeCompare(type.toString());
    final valueAux = _normalizeCompare(value);
    return typeStr.compareTo(valueAux) == 0;
  }, orElse: () { return null; });
}

String _normalizeCompare(String value) {
  if(value.contains('.')) {
    value = value.split('.').last;
  }
  return value;
}