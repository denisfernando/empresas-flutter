
class FormatText {
  
  /// Capitaliza a primeira letra de cada palavra do texto.
  /// Exemplo: flutter framework, terá o retorno Flutter Framework
  static String capitalizarPalavra(String text){
    if (text == null || text.trim().isEmpty) return '';

    if (text.trim().length == 1){
      return text.toLowerCase();
    }

    var retorno = '';
    text = text.toLowerCase().trim().trimLeft();
    text = text.replaceAll('  ', ' ');

    var arrayAux = text.split(' ');
    for (String value in arrayAux){
      var rest = value.substring(1);
      var init = value.substring(0, 1).toString().toUpperCase();

      retorno += '$init$rest ';
    }
    return retorno.trim();
  }

}
