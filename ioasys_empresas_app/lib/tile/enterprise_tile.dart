import 'package:flutter/material.dart';
import 'package:ioasys_empresas_app/library/util.dart';
import 'package:ioasys_empresas_app/library/util_style.dart';
import 'package:ioasys_empresas_app/model/enterprise_model.dart';
import 'package:ioasys_empresas_app/service/app_service.dart';
import 'package:ioasys_empresas_app/tab/enterprise_details_tab.dart';
export 'package:path/path.dart';

const double heightWidthImg = 70;

class EnterpriseTile extends StatelessWidget {
  final EnterpriseModel model;
  final int index;
  EnterpriseTile(this.model, this.index);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        color: getColor(),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Center(
            child: Column(
              children: [
                (model?.photo?.isNotEmpty ?? false)
                    ? Container(
                        height: heightWidthImg,
                        width: heightWidthImg,
                        child: Image(
                          image: NetworkImage("${Util.urlBase}/${model.photo}"),
                        ),
                      )
                    : SizedBox.shrink(),
                Text(
                  model.enterpriseName,
                  overflow: TextOverflow.ellipsis,
                  style: UtilStyle.textStyleCustom(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: (){
        AppService.instance.navigateTo(EnterpriseDetailsTab(model, index));
      },
    );
  }

  Color getColor() {
    final value = (index + 3) % 3;

    return value == 0
        ? color1
        : value == 1
            ? color2
            : color3;
  }

  Color get color1 {
    return Color.fromRGBO(121, 187, 202, 1);
  }

  Color get color2 {
    return Color.fromRGBO(235, 151, 151, 1);
  }

  Color get color3 {
    return Color.fromRGBO(144, 187, 129, 1);
  }
}
